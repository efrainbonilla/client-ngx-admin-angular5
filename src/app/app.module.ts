import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing, appRoutingProviders } from './app.routing';
import { NgxAdminLteModule } from 'ngx-admin-lte';

//components
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { MenuWidgetComponent } from './widgets/menu-widget/menu-widget.component';
import { HeaderWidgetComponent } from './widgets/header-widget/header-widget.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuWidgetComponent,
    HeaderWidgetComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgxAdminLteModule,
    routing
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
